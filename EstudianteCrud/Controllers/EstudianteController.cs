﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EstudianteCrud.Models;

namespace EstudianteCrud.Controllers
{
    public class EstudianteController : Controller
    {
        // GET: Estudiante
        public ActionResult Index()
        {
            EstudianteDBClase dbClase = new EstudianteDBClase();
            ModelState.Clear();
            return Json(dbClase.GetEstudiante(), JsonRequestBehavior.AllowGet);
        }

        // GET: Estudiante/Details/5
        public ActionResult Details(int id)
        {
            EstudianteDBClase edb = new EstudianteDBClase();
            return Json(edb.GetEstudiante().Find(emodel => emodel.Id == id), JsonRequestBehavior.AllowGet);
        }

        // GET: Estudiante/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Estudiante/Create
        [HttpPost]
        public ActionResult Create(EstudianteModel emodel)
        {
            try
            {
                // TODO: Add insert logic here
                if (ModelState.IsValid)
                {
                    EstudianteDBClase edb = new EstudianteDBClase();
                    if (edb.AgregarEstudiante(emodel))
                    {
                        ViewBag.Message = "Se agrego correctamente el estudiante";
                        ModelState.Clear();
                    }
                }
                return View();
            }
            catch
            {
                return View();
            }
        }

        // GET: Estudiante/Edit/5
        public ActionResult Edit(int id)
        {
            EstudianteDBClase edb = new EstudianteDBClase();
            return View(edb.GetEstudiante().Find(emodel => emodel.Id == id));
        }

        // POST: Estudiante/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, EstudianteModel emodel)
        {
            try
            {
                // TODO: Add update logic here
                EstudianteDBClase edb = new EstudianteDBClase();
                edb.actualizarEstudiante(emodel);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Estudiante/Delete/5
        public ActionResult Delete(int id)
        {
            try
            {
                EstudianteDBClase edb = new EstudianteDBClase();
                if (edb.deleteEstudiante(id))
                {
                    ViewBag.AlertMsg = "Estudiante eliminado correctamente";
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
            
        }

        // POST: Estudiante/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
