create database EstudianteDb;
use EstudianteDb;

create table EstudianteReg(
	id int not null primary key identity,
	name nvarchar(25) null,
	city nvarchar(25) null,
	address nvarchar(25) null
);

create procedure AddNewEstudiante(
	@name nvarchar(25),
	@city nvarchar(25),
	@address nvarchar(25)
)
as 
begin
	insert into EstudianteReg values(@name, @city, @address)
end

create procedure getAllEstudiantes
as
begin
	select * from EstudianteReg
end

create procedure updateEstudiante(
	@id int,
	@name nvarchar(25),
	@city nvarchar(25),
	@address nvarchar(25)
)
as
begin
	update EstudianteReg set
	name = @name,
	city = @city,
	address = @address where id = @id
end

-- eliminar estudiante
create procedure deleteEstudiante
(
	@id int
)
as
begin
	delete from EstudianteReg where id = @id
end