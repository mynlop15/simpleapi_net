﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace EstudianteCrud.Models
{
    public class EstudianteDBClase
    {
        private SqlConnection con;
        private void connection()
        {
            string conString = ConfigurationManager.ConnectionStrings["EstudianteConn"].ToString();
            con = new SqlConnection(conString);
        }

        //agregar estudiante
        public bool AgregarEstudiante(EstudianteModel emodel)
        {
            connection();
            SqlCommand cmd = new SqlCommand("AddNewEstudiante", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@name", emodel.Name);
            cmd.Parameters.AddWithValue("@city", emodel.City);
            cmd.Parameters.AddWithValue("@address", emodel.Address);

            con.Open();
            int i = cmd.ExecuteNonQuery();
            con.Close();

            if (i >= 1)
                return true;
            else
                return false;
        }

        //Ver estudiante
        public List<EstudianteModel> GetEstudiante()
        {
            connection();
            List<EstudianteModel> estudianteList = new List<EstudianteModel>();

            SqlCommand cmd = new SqlCommand("getAllEstudiantes", con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sd = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            con.Open();
            sd.Fill(dt);
            con.Close();

            foreach(DataRow dr in dt.Rows)
            {
                estudianteList.Add(
                    new EstudianteModel
                    {
                        Id = Convert.ToInt32(dr["id"]),
                        Name = Convert.ToString(dr["name"]),
                        City = Convert.ToString(dr["city"]),
                        Address = Convert.ToString(dr["address"])
                    }
                );
            }
            return estudianteList;
        }

        //actualizar estudiante
        public bool actualizarEstudiante(EstudianteModel emodel)
        {
            connection();
            SqlCommand cmd = new SqlCommand("updateEstudiante", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@id", emodel.Id);
            cmd.Parameters.AddWithValue("@name", emodel.Name);
            cmd.Parameters.AddWithValue("@city", emodel.City);
            cmd.Parameters.AddWithValue("@address", emodel.Address);

            con.Open();
            int i = cmd.ExecuteNonQuery();
            con.Close();

            if(i >= 1)
                return true;
            else
                return false;

        }

        //eliminar estudiante
        public bool deleteEstudiante(int id)
        {
            connection();
            SqlCommand cmd = new SqlCommand("deleteEstudiante", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@id", id);

            con.Open();
            int i = cmd.ExecuteNonQuery();
            con.Close();

            if (i >= 1)
                return true;
            else
                return false;
        }
    }
}