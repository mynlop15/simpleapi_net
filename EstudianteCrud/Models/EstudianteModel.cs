﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace EstudianteCrud.Models
{
    public class EstudianteModel
    {
        [Display(Name = "Id")]
        public int Id { get; set; }

        [Required(ErrorMessage = "El nombre es obligatorio")]
        public string Name { get; set; }

        [Required(ErrorMessage = "La ciudad es obligatoria")]
        public string City { get; set; }

        [Required(ErrorMessage = "La direccion es obligatoria")]
        public string Address { get; set; }
    }
}